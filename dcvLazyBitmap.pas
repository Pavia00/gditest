{+------------------------------------------------------------------------------+
  * License   : See "Licece.txt". The modified BSD License.
  * Project   : Delphi computer vision
  * The part  : dcvLazyBitmap
  * Purpose   : ������� ������.
  * Author    : ������ ���� ������������� (Pavia)
  * Copyright : 2016, ������ ���� ������������� (Pavia)
  * Version   : 24.12.2016
  * Home Page : http://
  * E-Mail    : delphicv@mail.ru
+------------------------------------------------------------------------------+
|                            License checksum                                  |
+------------------------------------------------------------------------------+
  * File name : "License.txt"
  * MD5       : 9e27395027bac84adf34a45d3d23ba38
  * SHA256:   : 23344671453008fa27343b7ff1d72efc14aaa714aaa7492659b4198cda59a183
  * GOST      : 475e5ff8eb6a91cd60845fc49f30819990f58bf55f7681f66b008f9ed4770ee5
+------------------------------------------------------------------------------+
|                            License notice                                    |
+------------------------------------------------------------------------------+
All Rights Reserved, by Pavia. For more information read Licece.txt or
email delphicv@mail.ru.

THE SOFTWARE IS PROVIDED "AS IS"...
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
+------------------------------------------------------------------------------+
+------------------------------------------------------------------------------+
}
unit dcvLazyBitmap;

interface

uses Windows, Classes, Graphics;

type
  TAColor=array[0..65536] of TColor;
  PAColor=^TAColor;
  PColor=^TColor;

  TLazy=record
     BMPRef:TBitmap;
     Origin:PAColor;
     LineRef:PAColor;
     LineSize:Integer;
     LastX,LastY:Integer;
     end;

  TLazyBitmap = class(TBitmap)
  private
    Lazy:TLazy;
    procedure SetHeight(Value: Integer); override;
    procedure SetWidth(Value: Integer); override;
  protected
  public
    procedure NewSize;
    procedure UpData;
    function GetPixel(x, y:Integer):TColor;
    procedure SetPixel(x, y:Integer; value:TColor);
    procedure Assign(Source: TPersistent);
    constructor Create; override;
   // destructor Destroy; override;

  published

  end;

implementation

{ TLazyBitmap }

// ������� GetPixel, �� ������� ������������, �� �������� ScanLine
// ! � ������� ��� �������� ������ �� ������� ��������
procedure TLazyBitmap.Assign(Source: TPersistent);
begin
  inherited;
  NewSize;
end;

constructor TLazyBitmap.Create;
begin
  inherited;
  {$IFDEF FPC}
  RawimageNeeded(true);
  PixelFormat:=pf32bit;
  {$ELSE}
  PixelFormat:=pf32bit;
  {$ENDIF}
  NewSize;
end;

function TLazyBitmap.GetPixel(x, y:Integer):TColor;
begin
With Lazy do
  begin
    if LastY=Y then Result:=LineRef^[X]
       else
       IF LastX=X then
          begin
            Inc(PByte(LineRef), LineSize*(Y-LastY));
            Result:=LineRef^[X];
          end else
          begin
            LineRef:=Origin;
            Inc(PByte(LineRef), LineSize*Y);
            Result:=LineRef^[X];
          end;
  LastX:=X;
  LastY:=Y;
  end;

end;

procedure TLazyBitmap.NewSize;
begin
  if Self.Height<2 then Height:=2;
  if Self.Width<1 then Width:=1;

  With Lazy do
    begin
    {$IFDEF FPC}
    LineSize:=Self.RawImage.Description.BytesPerLine;
    Origin:=PACOlor(Self.RawImage.Data);
    {$ELSE}
    LineSize:=DWord(Self.ScanLine[1])-DWord(Self.ScanLine[0]);
    Origin:=PAColor(Self.ScanLine[0]);
    {$ENDIF}
    LineRef:=Origin;
    LastX:=0;
    LastY:=0;
    end;
end;

procedure TLazyBitmap.SetHeight(Value: Integer);
begin
  inherited;
  NewSize;
end;

procedure TLazyBitmap.SetPixel(x, y:Integer; value:TColor);
begin
With Lazy do
  begin
    if LastY=Y then LineRef^[X]:=value
       else
       IF LastX=X then
          begin
            Inc(PByte(LineRef), LineSize*(Y-LastY));
            LineRef^[X]:=Value;
          end else
          begin
            LineRef:=Origin;
            Inc(PByte(LineRef), LineSize*Y);
            LineRef^[X]:=Value;
          end;
  LastX:=X;
  LastY:=Y;
  end;

end;

procedure TLazyBitmap.SetWidth(Value: Integer);
begin
  inherited;
  NewSize;
end;

procedure TLazyBitmap.UpData;
begin
 NewSize;
end;

end.
