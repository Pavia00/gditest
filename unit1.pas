unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, gl,
  SysUtils, FileUtil, OpenGLContext, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Spin, Buttons, math,OpenCV,IPL, dcvLazyBitmap;

type
  {define doubleprec}
  {$ifdef doubleprec}
  GDBReal=double;
  {$else}
  GDBReal=single;
  {$endif}
  PGDBVertex=^GDBVertex;
  GDBVertex=record
                  x,y,z:GDBReal;
  end;
  PGDBvertex4=^GDBvertex4;
  GDBvertex4=packed record
                  x,y,z,w:GDBReal;
              end;
  DVector4=packed array[0..3]of GDBReal;
  PDMatrix4=^DMatrix4;
  DMatrix4=packed array[0..3]of DVector4;


  { TForm1 }

  TForm1 = class(TForm)
    Label5: TLabel;
    LazyWP: TPanel;
    BitBtn1: TBitBtn;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    GDIPlusWP: TPanel;
    GDIWP: TPanel;
    AGGWP: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label_name: TLabel;
    Memo1: TMemo;
    OpenGLWP: TOpenGLControl;
    SpinEdit1: TSpinEdit;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    procedure CheckBox2Change(Sender: TObject);
    procedure GDIPaint(Sender: TObject);
    procedure AlexBypePaint(Sender: TObject);
    procedure ZubPaint(Sender: TObject);
    procedure LazyPaint(Sender: TObject);
    procedure OpenGLPaint(Sender: TObject);
    procedure runtest(Sender: TObject);
    procedure _formCreate(Sender: TObject);
    procedure WPClick(Sender: TObject);
    procedure processresult;
  private
    { private declarations }
  public
    { public declarations }
    needresult:boolean;
    testcount:integer;
  end;

const
  eps=1e-14;
  OneMatrix: DMatrix4 = ((1, 0, 0, 0),
                          (0, 1, 0, 0),
                          (0, 0, 1, 0),
                          (0, 0, 0, 1));

var
  Form1: TForm1;
  needtransform:boolean;
  matr:DMatrix4;

implementation

{$R *.lfm}

{ TForm1 }
function CreateScaleMatrix(const x,y,z:GDBReal): DMatrix4;
begin
  Result := onematrix;
  Result[0, 0] := x;
  Result[1, 1] := y;
  Result[2, 2] := z;
  Result[3, 3] := 1;
end;

function CreateRotationMatrixZ(const Sine, Cosine: GDBReal): DMatrix4;
begin
  Result := Onematrix;
  Result[0, 0] := Cosine;
  Result[1, 1] := Cosine;
  Result[1, 0] := -Sine;
  Result[0, 1] := Sine;
end;
function MatrixMultiply(const M1, M2: DMatrix4): DMatrix4;

var I, J: Integer;
    TM: DMatrix4;

begin
  for I := 0 to 3 do
    for J := 0 to 3 do
      TM[I, J] := M1[I,0] * M2[0,J] +
                  M1[I,1] * M2[1,J] +
                  M1[I,2] * M2[2,J] +
                  M1[I,3] * M2[3,J];
  Result := TM;
end;
function CreateTranslationMatrix(const x,y,z:GDBReal): DMatrix4;
begin
  Result := onematrix;
  Result[3, 0] := x;
  Result[3, 1] := y;
  Result[3, 2] := z;
  Result[3, 3] := 1;
end;
function VectorTransform(const V:GDBVertex4;const M:DMatrix4):GDBVertex4;
var TV: GDBVertex4;
begin
  TV.X := V.X * M[0, 0] + V.y * M[1, 0] + V.z * M[2, 0] + V.w * M[3, 0];
  TV.Y := V.X * M[0, 1] + V.y * M[1, 1] + V.z * M[2, 1] + V.w * M[3, 1];
  TV.z := V.x * M[0, 2] + V.y * M[1, 2] + V.z * M[2, 2] + V.w * M[3, 2];
  TV.W := V.x * M[0, 3] + V.y * M[1, 3] + V.z * M[2, 3] + V.w * M[3, 3];

  Result := TV
end;
procedure normalize4d(var tv:GDBVertex4);
begin
  if abs(tv.w)>eps then
  if abs(abs(tv.w)-1)>eps then
  begin
  tv.x:=tv.x/tv.w;
  tv.y:=tv.y/tv.w;
  tv.z:=tv.z/tv.w;
  end;
end;
function VectorTransform3D(const V:GDBVertex;const M:DMatrix4):GDBVertex;
var TV: GDBVertex4;
begin
  pgdbvertex(@tv)^:=v;
  tv.w:=1;
  tv:=VectorTransform(tv,m);

  normalize4d(tv);

  Result := pgdbvertex(@tv)^
end;
function GetRandomVertex(w,h,z:integer):GDBVertex;
begin
     result.x:=random(w);
     result.y:=random(h);
     result.z:=random(z);
     if needtransform then
     begin
          result:=VectorTransform3D(result,matr);
     end;
end;

procedure TForm1.processresult;
var
   pvendor,prender,pversion:pchar;
begin
     if needresult then
     begin
     pvendor:=glGetString(GL_VENDOR);
     prender:=glGetString(GL_RENDERER);
     pversion:=glGetString(GL_VERSION);

     inc(testcount);
     if testcount=4 then
     begin
          Memo1.Lines.Text:='OpenGL driver info: '+pvendor+' '+prender+' '+pversion+#13#10+
                            'Draw '+inttostr(SpinEdit1.Value)+' random lines'+#13#10+
                            Label1.Caption+#13#10+
                            Label2.Caption+#13#10+
                            Label3.Caption+#13#10+
                            Label4.Caption+#13#10+
                            Label5.Caption;
          needresult:=false;
     end;
     end;

end;

procedure TForm1.GDIPaint(Sender: TObject);
var
  i:integer;
  w,h:integer;
  LPTime:Tdatetime;
  tv1,tv2:GDBVertex;
begin
     w:=TPanel(Sender).Width;
     h:=TPanel(Sender).Height;
     needtransform:=CheckBox1.Checked;
     LPTime:=now();
     for i:=1 to SpinEdit1.Value do
     begin
          tv1:=GetRandomVertex(w,h,w);
          tv2:=GetRandomVertex(w,h,w);
          TPanel(Sender).canvas.Line(round(tv1.x),round(tv1.y),round(tv2.x),round(tv2.y));
     end;
     lptime:=now()-LPTime;
     Label1.Caption:='Canvas: '+inttostr(round(lptime*10e7))+'msec';
     processresult;
end;

procedure TForm1.CheckBox2Change(Sender: TObject);
begin

end;

procedure TForm1.AlexBypePaint(Sender: TObject);
var
  i:integer;
  w,h:integer;
  LPTime:Tdatetime;
  tv1,tv2:GDBVertex;

Function InR(AA,B,C:Longint):Boolean;
begin
InR:=((AA>=B) And (AA<=C));
End;

// Только 24 Бита !
var BL:Integer; //Количество байт в строке битмапа 
Procedure SetPix(Var BB:TBitmap;X,Y,C:Integer);
Type
TA=Array[0..1] of byte;
var
PA:^TA;
n:integer;
begin
{ Без прверки ясный пеннь быстрее но немного ....
if bb = NIL then exit;
if not InR(x,0,bb.Width-1) then exit;
if not InR(y,0,bb.Height-1) then exit;
 }
//Bb.BeginUpdate; ;
pa:=///bb.ScanLine[Y];
Pointer(BB.RawImage.Data+Y*BL);
//((BB.Width+1)*3));
N:=X*3;
pa^[n]  :=Blue(C);
pa^[n+1]:=Green(C);
pa^[n+2]:=red(C);
//Bb.EndUpdate;
end;

procedure LazyLine(BB:TBitmap; x1,y1,x2,y2:Integer); overload;
var e,i,x,y,dx,dy,sx,sy:Integer;
 step:Integer;
begin
step:=0;
//if (BB=nil)then exit;
if step=0 then
 begin
 x:=x1;
 y:=y1;
 dx:=Abs(x2-x1);
 dy:=Abs(y2-y1);
 sx:=Sign(x2-x1);
 sy:=Sign(y2-y1);

 if (dx=0) and (dy=0) then
  begin
  //Bmp.SetPixel
  SetPix(BB,x,y, clRed);
  Exit;
  end;
 if dy<dx then
  begin
  e:=2*dy-dx;
  i:=1;
  repeat
  SetPix(BB,x,y, clRed);
  while e>=0 do
   begin
   y:=y+sy;
   e:=e-2*dx;
   end;
  x:=x+sx;
  e:=e+2*dy;
  i:=i+1;
  until i>dx;
   SetPix(BB,x,y, clRed);
  end else
  begin
  e:=2*dx-dy;
  i:=1;
  repeat
   SetPix(BB,x,y, clRed);
   while e>=0 do
    begin
    x:=x+sx;
    e:=e-2*dy;
    end;
   y:=y+sy;
   e:=e+2*dx;
   i:=i+1;
   until i>dy;
  SetPix(BB,x,y, clRed);
  end;
 end;
end;

Const BB:TBitmap=nil;
Var
  cs: CvSize;
  cvCP1,cvCP2:cvPoint;

const
  frame: PIplImage=nil ;
  begin

     w:=TPanel(Sender).Width;
     h:=TPanel(Sender).Height;
     needtransform:=CheckBox1.Checked;
      BB:=TBitmap.Create;
      bb.PixelFormat:=pf24bit;
      bb.SetSize(w,h);
      BL:=BB.RawImage.Description.BitsPerLine div 8;
//Флаг OpenCV 
If not CheckBox2.Checked then begin
     LPTime:=now();
     Bb.BeginUpdate;
     for i:=1 to SpinEdit1.Value do
     begin
          tv1:=GetRandomVertex(w,h,w);
          tv2:=GetRandomVertex(w,h,w);
          LazyLine(BB,round(tv1.x),round(tv1.y),round(tv2.x),round(tv2.y));
     end;
       Bb.EndUpdate;
       TPanel(Sender).Canvas.Draw(0,0,bb);
       lptime:=now()-LPTime;
       end else begin
//*******************************************************
//*              Подключение Опен ЦВ                    *
//*******************************************************

Cs.width:=w;
Cs.height:=h;

if Frame = nil then  Frame:= cvCreateImage( cs, 8, 3 )
 else
 begin
 cvReleaseImage(Frame);
 Frame:= cvCreateImage( cs, 8, 3 )
 end;
FillChar(Frame^.ImageData^, Frame^.ImageSize,0);
LPTime:=now();
for i:=1 to SpinEdit1.Value do
begin
     tv1:=GetRandomVertex(w,h,w);
     tv2:=GetRandomVertex(w,h,w);

 cvCP1.x:=round(tv1.x); cvCP1.y:=round(tv1.y);
 cvCP2.x:=round(tv2.x); cvCP2.y:=round(tv2.y);
 cvLine(Frame, cvCP1, cvCP2, CV_RGB(0, 255, 0));
end;
 IplImage2Bitmap(Frame,BB);
 lptime:=now()-LPTime;
   // Иначе было бы не честно по отношению к  OpenCV
   // в чистом OpenCV была бы одна пересылка
   // Но разница менее 10 msec (на HP635)
   TPanel(Sender).Canvas.Draw(0,0,bb);
  end;
     bb.Free;
  If not CheckBox2.Checked then
  Label2.Caption:='Alex Byte : '+inttostr(round(lptime*10e7))+'msec'
  Else Label2.Caption:='OpenCV : '+inttostr(round(lptime*10e7))+'msec';
     processresult;
end;



procedure TForm1.ZubPaint(Sender: TObject);

  Procedure Swap(var x,y:Integer);
  var t:Integer;
  begin
    t:=x;x:=y;y:=t;
  end;

  Procedure putpoint(pd:pbyte;cl:Longint);
  type TRGB=packed record r,g,b:byte;end;
      PTRGB=^TRGB;
  var
     t:byte;
  begin
    t:=PTRGB(@cl)^.b;
    PTRGB(@cl)^.b:=PTRGB(@cl)^.r;
    PTRGB(@cl)^.r:=t;
    PTRGB(pd)^:=PTRGB(@cl)^;
  end;

  function Align4(value:integer):integer;
  const cAlign:DWord=4;
  begin
  result:=(DWord(value)+(cAlign-1)) and not (cAlign-1);
  end;

  Procedure Line(pdata:pbyte;x1,y1,x2,y2:integer;w,h:integer;cl:tcolor);
  var dx,dy,i,sx,sy,check,e:integer;
  const bytesperpixel=3;
  begin
      pdata:=pdata+x1*bytesperpixel+y1*Align4(w*bytesperpixel);
      dx:=abs(x1-x2);
      dy:=abs(y1-y2);
      sx:=Sign(x2-x1)*bytesperpixel;
      sy:=Sign(y2-y1)*Align4(w*bytesperpixel);
      check:=0;
      if dy>dx then begin
          Swap(dx,dy);
          check:=1;
      end;
      e:= 2*dy - dx;
      for i:=0 to dx do begin
          putpoint(pdata,cl);
          if e>=0 then begin
              if check=1 then
                             inc(pdata,sx)
                         else
                             inc(pdata,sy);
              dec(e,2*dx);
          end;
          if check=1 then
                         inc(pdata,sy)
                     else
                         inc(pdata,sx);
          inc(e,2*dy);
      end;
   end;

var
  i:integer;
  w,h:integer;
  LPTime:Tdatetime;
  Bitmap1: TBitmap;
  tv1,tv2:GDBVertex;
  p:pbyte;
  cl:LongInt;
begin
     w:=TPanel(Sender).Width;
     h:=TPanel(Sender).Height;
     needtransform:=CheckBox1.Checked;
     Bitmap1:=TBitmap.Create;
     Bitmap1.PixelFormat:=pf24bit;
     Bitmap1.SetSize(w,h);
     Bitmap1.Canvas.Brush.Color:=clRed;
     Bitmap1.Canvas.FillRect(0,0,w,h);
     cl:=ColorToRGB(clBlue);

     LPTime:=now();
     Bitmap1.BeginUpdate;
     for i:=1 to SpinEdit1.Value do
     begin
          tv1:=GetRandomVertex(w,h,w);
          tv2:=GetRandomVertex(w,h,w);
          Line(Bitmap1.RawImage.Data,round(tv1.x),round(tv1.y),round(tv2.x),round(tv2.y),w,h,cl);
     end;
     Bitmap1.EndUpdate;
     TPanel(Sender).canvas.Draw(0,0,Bitmap1);
     lptime:=now()-LPTime;
     Label3.Caption:='Zub Byte : '+inttostr(round(lptime*10e7))+'msec';
     Bitmap1.free;
     processresult;
end;

procedure TForm1.LazyPaint(Sender: TObject);
  procedure LazyLine(Bmp:TLazyBitmap; x1,y1,x2,y2:Integer; const color:TColor); overload;
  var e,i,x,y,dx,dy,sx,sy:Integer;
   step:Integer;
  begin
  step:=0;
  if (Bmp=nil)then exit;
  if step=0 then
   begin
   x:=x1;
   y:=y1;
   dx:=Abs(x2-x1);
   dy:=Abs(y2-y1);
   sx:=Sign(x2-x1);
   sy:=Sign(y2-y1);

   if (dx=0) and (dy=0) then
    begin
    Bmp.SetPixel(x,y, color);
    Exit;
    end;
   if dy<dx then
    begin
    e:=2*dy-dx;
    i:=1;
    repeat
    Bmp.SetPixel(x,y, color);
    while e>=0 do
     begin
     y:=y+sy;
     e:=e-2*dx;
     end;
    x:=x+sx;
    e:=e+2*dy;
    i:=i+1;
    until i>dx;
    Bmp.SetPixel(x,y, color);
    end else
    begin
    e:=2*dx-dy;
    i:=1;
    repeat
    Bmp.SetPixel(x,y, color);
     while e>=0 do
      begin
      x:=x+sx;
      e:=e-2*dy;
      end;
     y:=y+sy;
     e:=e+2*dx;
     i:=i+1;
     until i>dy;
     Bmp.SetPixel(x,y, color);
    end;
   end;
  end;

var
  i:integer;
  w,h:integer;
  LPTime:Tdatetime;
  LazyBitmap: TLazyBitmap;
  tv1,tv2:GDBVertex;
  p:pbyte;
  cl:LongInt;
begin
     w:=TPanel(Sender).Width;
     h:=TPanel(Sender).Height;
     needtransform:=CheckBox1.Checked;
     LazyBitmap:=TLazyBitmap.Create;
     LazyBitmap.PixelFormat:=pf32bit;
     LazyBitmap.SetSize(w,h);
     LazyBitmap.Canvas.Brush.Color:=clAqua;
     LazyBitmap.Canvas.FillRect(0,0,w,h);
     cl:=ColorToRGB(clBlue);

     LPTime:=now();
     LazyBitmap.BeginUpdate;
     LazyBitmap.UpData;
     for i:=1 to SpinEdit1.Value do
     begin
          tv1:=GetRandomVertex(w,h,w);
          tv2:=GetRandomVertex(w,h,w);
          LazyLine(LazyBitmap,round(tv1.x),round(tv1.y),round(tv2.x),round(tv2.y),cl);
     end;
     LazyBitmap.EndUpdate;
     TPanel(Sender).canvas.Draw(0,0,LazyBitmap);
     lptime:=now()-LPTime;
     Label4.Caption:='Lazy : '+inttostr(round(lptime*10e7))+'msec';
     LazyBitmap.free;
     processresult;
end;

procedure TForm1.OpenGLPaint(Sender: TObject);
var
  i:integer;
  w,h:integer;
  LPTime:Tdatetime;
  Bitmap1: TBitmap;
  tv1,tv2:GDBVertex;
begin
     w:=TOpenGLControl(Sender).Width;
     h:=TOpenGLControl(Sender).Height;
     needtransform:={CheckBox1.Checked}false;

     LPTime:=now();
     glClearColor(1.0, 1.0, 1.0, 1.0);
     glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
     glEnable(GL_DEPTH_TEST);

     glMatrixMode(GL_PROJECTION);
     glLoadIdentity();
     glortho(0,w,0,h,-1,1);
     glMatrixMode(GL_MODELVIEW);
     glLoadIdentity();
     glViewport(0, 0, w, h);
     glColor4f(0, 0, 0, 0);

     if CheckBox1.Checked then
                              glMultMatrixf(@matr);

     glBegin(gl_lines);
     for i:=1 to SpinEdit1.Value do
     begin
          tv1:=GetRandomVertex(w,h,0);
          tv2:=GetRandomVertex(w,h,0);
          {$ifdef doubleprec}
          glVertex3dv(@tv1);
          glVertex3dv(@tv2);
          {$else}
          glVertex3fv(@tv1);
          glVertex3fv(@tv2);
          {$endif}
     end;
     glEnd;
     glFlush;
     OpenGLWP.SwapBuffers;
     lptime:=now()-LPTime;
     Label5.Caption:='OpenGL: '+inttostr(round(lptime*10e7))+'msec';
     processresult;
end;

procedure TForm1.runtest(Sender: TObject);
begin
     needresult:=true;
     testcount:=0;
     self.Invalidate;
end;

procedure TForm1.WPClick(Sender: TObject);
begin
     TPanel(Sender).Invalidate;
end;

procedure TForm1._formCreate(Sender: TObject);
const
  angle=15*pi/180;
begin
     matr:=CreateTranslationMatrix(5,5,0);
     matr:=MatrixMultiply(matr,CreateRotationMatrixZ(sin(angle),cos(angle)));
     matr:=MatrixMultiply(matr,CreateScaleMatrix(1.5,1.5,1.5));
     GDIWP.OnPaint:=@Form1.GDIPaint;
     GDIWP.OnClick:=@Form1.WPClick;
     GDIPlusWP.OnPaint:=@Form1.AlexBypePaint;
     GDIPlusWP.OnClick:=@Form1.WPClick;
     AGGWP.OnPaint:=@Form1.ZubPaint;
     AGGWP.OnClick:=@Form1.WPClick;
     OpenGLWP.OnPaint:=@Form1.OpenGLPaint;
     OpenGLWP.OnClick:=@Form1.WPClick;
     LazyWP.OnPaint:=@Form1.LazyPaint;
     LazyWP.OnClick:=@Form1.WPClick;
     needresult:=false;
end;

end.
